//
//  ASAsteroid.h
//  Asteroids
//
//  Created by Lisa Wen on 10/14/14.
//
//

#import "ASDrawable.h"

@interface ASAsteroid : ASDrawable

- (id) initLarge;
- (void) update;

@end
