//
//  ASAsteroid.m
//  Asteroids
//
//  Created by Lisa Wen on 10/14/14.
//
//

#import "ASAsteroid.h"
#import "ASDrawable.h"

@implementation ASAsteroid

- (id) initLarge {
    self = [super initWithImage:[NSImage imageNamed:@"asteroidLarge.png"]];
    return self;
}

- (void) update {
    
}

@end
