//
//  YWChatBot.m
//  ChatBot
//
//  Created by Lisa Wen on 10/7/14.
//
//

#import "YWChatBot.h"

@implementation YWChatBot

+ (NSString *)screenName {
    return @"Administrator";
}

- (void) respondToChatMessage:(NSString *)chatMessage {
    if ([chatMessage isEqual:@"hello"]) {
        [self sendMessage:@"hello"];
    }
    if ([chatMessage isEqual:@"date"]) {
        [self sendMessage:[[NSDate date] description]];
    }
    
    if ([chatMessage hasPrefix:@"remember"]) {
        rememberedString = [chatMessage retain];
    }
    if ([chatMessage isEqual:@"recall"]) {
        [self sendMessage:rememberedString];
    }
    
    if ([chatMessage hasPrefix:@"timer"]) {
        float time = [[chatMessage substringFromIndex:6] floatValue];
        [NSTimer scheduledTimerWithTimeInterval:time
                                         target:self
                                       selector:@selector(timeTriggered)
                                       userInfo:nil
                                        repeats:NO];
    }
}

- (void) timeTriggered {
    [self sendMessage:@"ding!"];
}

@end
