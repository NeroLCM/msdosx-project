//
//  FCStackCalculatorController.h
//  FractionCalculator
//
//  Created by Lisa Wen on 10/7/14.
//
//

#import <Foundation/Foundation.h>
#import "FCStack.h"

@interface FCStackCalculatorController : NSObject {
    FCStack* stack;
}

- (id) init;

- (void) respondToInput: (NSString *) input;


@end
