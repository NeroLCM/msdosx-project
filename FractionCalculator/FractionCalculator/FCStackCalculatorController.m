//
//  FCStackCalculatorController.m
//  FractionCalculator
//
//  Created by Lisa Wen on 10/7/14.
//
//

#import "FCStackCalculatorController.h"
#import "FCConsole.h"
#import "Fraction.h"

@implementation FCStackCalculatorController

- (id) init {
    
    self = [super init];
    
    if (self) {
        stack = [[FCStack alloc] init];
    }
    
    return self;
}

- (void) respondToInput: (NSString *) input
{
    if ([input hasPrefix:@"push"]) {
        NSString *fractionString = [input substringFromIndex: 5];
        NSUInteger slash = [fractionString rangeOfString: @"/"].location;
        
        int numer = [[fractionString substringToIndex: slash] intValue];
        int denom = [[fractionString substringFromIndex: slash + 1] intValue];
        
        Fraction *fraction = [[Fraction alloc] initWithNumerator: numer andDenominator: denom];
        
        [stack push:fraction];
    }
    
    if ([input hasPrefix:@"pop"]) {
        [stack pop];
    }
    
    if ([input hasPrefix:@"clear"]) {
        [stack clear];
    }
    
    
    if ([input isEqual:@"+"]) {
        Fraction* first = [stack firstOperand];
        Fraction* second = [stack secondOperand];
        
        Fraction* result = [first add:second];
        
        [stack pop];
        [stack pop];
        [stack push:result];
    }
    
    if ([input isEqual:@"-"]) {
        Fraction* first = [stack firstOperand];
        Fraction* second = [stack secondOperand];
        
        Fraction* result = [first subtract:second];
        
        [stack pop];
        [stack pop];
        [stack push:result];
    }
    
    if ([input isEqual:@"*"]) {
        Fraction* first = [stack firstOperand];
        Fraction* second = [stack secondOperand];
        
        Fraction* result = [first multiply:second];
        
        [stack pop];
        [stack pop];
        [stack push:result];
    }
    
    if ([input isEqual:@"/"]) {
        Fraction* first = [stack firstOperand];
        Fraction* second = [stack secondOperand];
        
        Fraction* result = [first divide:second];
        
        [stack pop];
        [stack pop];
        [stack push:result];
    }
    
    if ([input isEqual:@"invert"]) {
        Fraction* fraction = [stack topOperand];
        
        Fraction* result = [fraction inverse];
        
        [stack pop];
        [stack push:result];
    }
    
    [stack print];
}

@end
