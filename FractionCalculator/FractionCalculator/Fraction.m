//
//  Fraction.m
//  FractionCalculator
//
//  Created by Kevin Jorgensen on 7/16/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "Fraction.h"


int GCD(int a, int b)
{
    int tmp;
    while ( a != 0 )
    {
        tmp = a;
        a = b % a;
        b = tmp;
    }
    return b;
}


@implementation Fraction


- (id) initWithNumerator: (int) numer andDenominator: (int) denom
{
    self = [super init];
    
    if (self)
    {
        numerator = numer;
        denominator = denom;
    }
    
    return self;
}


- (NSString*) description
{
    return [NSString stringWithFormat: @"%d/%d", numerator, denominator];
}


- (int) numerator
{
    return numerator;
}


- (int) denominator
{
    return denominator;
}


- (Fraction *) add: (Fraction *) otherFraction
{    
    int resultden = denominator * [otherFraction denominator];
    int resultnum = numerator * [otherFraction denominator] + denominator * [otherFraction numerator];
    Fraction* newFraction =[[Fraction alloc] initWithNumerator:resultnum andDenominator:resultden];
    [newFraction reduce];
    return newFraction;
}


- (Fraction *) subtract: (Fraction *) otherFraction
{
    int resultden = denominator * [otherFraction denominator];
    int resultnum = numerator * [otherFraction denominator] - denominator * [otherFraction numerator];
    Fraction* newFraction =[[Fraction alloc] initWithNumerator:resultnum andDenominator:resultden];
    [newFraction reduce];
    return newFraction;
}


- (Fraction *) multiply: (Fraction *) otherFraction
{
    int resultden = denominator * [otherFraction denominator];
    int resultnum = numerator * [otherFraction numerator];
    Fraction* newFraction =[[Fraction alloc] initWithNumerator:resultnum andDenominator:resultden];
    [newFraction reduce];
    return newFraction;
}


- (Fraction *) divide: (Fraction *) otherFraction
{
    return [otherFraction multiply:[self inverse]];
}


- (void) reduce
{
    int gcd = GCD(numerator, denominator);
    numerator /= gcd;
    denominator /= gcd;
}


- (Fraction *) inverse
{
    Fraction* newFraction = [[Fraction alloc] initWithNumerator:denominator andDenominator:numerator];
    [newFraction reduce];
    return newFraction;
}


@end
