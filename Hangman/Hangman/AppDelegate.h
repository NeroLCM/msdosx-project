//
//  AppDelegate.h
//  Hangman
//
//  Created by Lisa Wen on 10/12/14.
//  Copyright (c) 2014 Yuanfeng Wen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

