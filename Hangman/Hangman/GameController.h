//
//  GameController.h
//  Hangman
//
//  Created by Lisa Wen on 10/13/14.
//  Copyright (c) 2014 Yuanfeng Wen. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HangmanWords.h"

@interface GameController : NSObject {
    HangmanWords *wordGenerator;
    NSString *word;
    int remainingAttempts;
    
    NSString *guessed;
};

-(int) attemptsLeft;
-(NSString *) getWord;
-(NSString *) wordsGuessed;
-(BOOL) isGuessed: (NSString *) wordGuessed;

-(BOOL) checkWord: (NSString *) wordGuessed;
-(void) resetWord;
-(NSString *) giveUp;
-(NSString *) changeLabel: (NSString *)preLabel withGuess: (NSString *) guess;

@end
