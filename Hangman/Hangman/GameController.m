//
//  GameController.m
//  Hangman
//
//  Created by Lisa Wen on 10/13/14.
//  Copyright (c) 2014 Yuanfeng Wen. All rights reserved.
//

#import "GameController.h"
#import "HangmanWords.h"

@implementation GameController

-(id) init {
    self = [super init];
    wordGenerator = [[HangmanWords alloc] init];
    
    return self;
}


-(int) attemptsLeft {
    return remainingAttempts;
}

-(NSString *) getWord {
    return word;
}

-(NSString *) wordsGuessed {
    return guessed;
}

-(BOOL) isGuessed: (NSString *) wordGuessed {
    wordGuessed = [wordGuessed uppercaseString];
    
    if ([guessed containsString:wordGuessed]) {
        return YES;
    }
    return NO;
}


-(BOOL) checkWord: (NSString *) wordGuessed {
    wordGuessed = [wordGuessed uppercaseString];
    guessed = [guessed stringByAppendingString:wordGuessed];
    
    if ([word containsString:wordGuessed]) {
        return YES;
    }
    remainingAttempts--;
    return NO;
}

-(void) resetWord {
    word = [[wordGenerator getWord] uppercaseString];
    remainingAttempts = 6;
    guessed = @"";
}

-(NSString *)giveUp {
    remainingAttempts = 0;
    return word;
}

-(NSString *) changeLabel: (NSString *) preLabel withGuess: (NSString*) guess {
    if (preLabel == nil) {
        NSString* text = @"A";
        for (int i = 0; i < [word length]; i++) {
            if ([[[word substringToIndex:i+1] substringFromIndex:i]compare:@" "]) {
                text = [[text substringToIndex:i] stringByAppendingString:@"_"];
            } else {
                text = [[text substringToIndex:i] stringByAppendingString:@" "];
            }
        }
        return text;
    }
    
    guess = [guess uppercaseString];
    for (int i = 0; i < [word length]; i++) {
        NSString * character = [[word substringToIndex:i+1] substringFromIndex:i];
        if ([guess isEqualToString:character]) {
            preLabel = [[[preLabel substringToIndex:i] stringByAppendingString:character] stringByAppendingString:[preLabel substringFromIndex:i + 1]];
        }
    }
    return preLabel;
}

@end
