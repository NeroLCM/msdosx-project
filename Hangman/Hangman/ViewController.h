//
//  ViewController.h
//  Hangman
//
//  Created by Lisa Wen on 10/12/14.
//  Copyright (c) 2014 Yuanfeng Wen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GameController.h"

@interface ViewController : UIViewController {
    GameController * gameController;
    
    __weak IBOutlet UIImageView *img;
    __weak IBOutlet UILabel *answerLabel;
    __weak IBOutlet UILabel *deathLabel;
    IBOutlet UITextField *input;
}

-(IBAction)giveUp:(id)sender;
-(IBAction)wordGuessed:(id)sender;
-(IBAction)reset:(id)sender;
- (IBAction)hintRequested:(id)sender;

@end