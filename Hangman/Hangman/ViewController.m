//
//  ViewController.m
//  Hangman
//
//  Created by Lisa Wen on 10/12/14.
//  Copyright (c) 2014 Yuanfeng Wen. All rights reserved.
//

#import "ViewController.h"
#import "GameController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    gameController = [[GameController alloc] init];
    [img setImage:[UIImage imageNamed:@"hang0.jpg"]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)giveUp:(id) sender {
    if ([[deathLabel text] isEqualToString:@"Hang Yourself!"] || [[deathLabel text] isEqualToString:@"You Succeed!"]) {
        return;
    }
    
    NSString *word = [gameController giveUp];
    
    [answerLabel setText:word];
    [deathLabel setText:@"BANG!"];
    [img setImage:[UIImage imageNamed:@"hang6.jpg"]];
}

- (IBAction)wordGuessed:(id)sender {
    if ([[deathLabel text] isEqualToString:@"Hang Yourself!"] || [[deathLabel text] isEqualToString:@"BANG!"]) {
        return;
    }
    
    NSString *wordGuessing = [input text];
    [input setText:@""];
    
    if ([gameController isGuessed:wordGuessing]) {
        [deathLabel setText:@"Repeated Guess!"];
        return;
    }
    
    if ([wordGuessing length] != 1) {
        [deathLabel setText:@"Input a SINGLE char!"];
        return;
    }
    
    BOOL correct = [gameController checkWord:wordGuessing];
    
    if (correct) {
        NSString* newLabel = [gameController changeLabel:[answerLabel text] withGuess:wordGuessing];
        [answerLabel setText:newLabel];
        
        if ([newLabel isEqualToString:[gameController getWord]]) {
            [deathLabel setText:@"You Succeed!"];
            return;
        }
        
        [deathLabel setText:@"Correct!"];
    } else {
        int attemptsLeft = [gameController attemptsLeft];
        if (!attemptsLeft) {
            [self giveUp:nil];
            return;
        }
        
        [deathLabel setText:[@"Wrong! Attempts Left:" stringByAppendingString:[NSString stringWithFormat:@"%d", attemptsLeft]]];
        [img setImage:[UIImage imageNamed:[[@"hang" stringByAppendingString:[NSString stringWithFormat:@"%d", 6 - attemptsLeft]] stringByAppendingString:@".jpg"]]];
    }
}

- (IBAction)reset:(id) sender {
    [gameController resetWord];
    
    [img setImage:[UIImage imageNamed:@"hang0.jpg"]];
    [deathLabel setText:[@"Attempts Left:" stringByAppendingString:[NSString stringWithFormat:@"%d",[gameController attemptsLeft]]]];
    [answerLabel setText: [gameController changeLabel:nil withGuess:nil]];
}

- (IBAction)hintRequested:(id)sender {
    if ([[deathLabel text] isEqualToString:@"Hang Yourself!"] || [[deathLabel text] isEqualToString:@"BANG!"] || [[deathLabel text] isEqualToString:@"You Succeed!"]) {
        return;
    }
    
    [deathLabel setText:[@"Guessed:" stringByAppendingString:[gameController wordsGuessed]]];
}

@end
