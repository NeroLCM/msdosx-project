//
//  main.m
//  Hangman
//
//  Created by Lisa Wen on 10/12/14.
//  Copyright (c) 2014 Yuanfeng Wen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
