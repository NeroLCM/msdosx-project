//
//  main.m
//  Sudoku
//
//  Created by Lisa Wen on 12/10/14.
//  Copyright (c) 2014 Awww. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[]) {
    return NSApplicationMain(argc, argv);
}
