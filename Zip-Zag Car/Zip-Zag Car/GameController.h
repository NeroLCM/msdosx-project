//
//  GameController.h
//  Zig-Zag Car
//
//  Created by Lisa Wen on 12/10/14.
//  Copyright (c) 2014 Final Project. All rights reserved.
//

#import <Foundation/Foundation.h>
#define TIME 0.02
@interface GameController : NSObject {
    
    
}
@property (nonatomic) int levels;
@property (nonatomic) int score;
@property (nonatomic) BOOL succeed;
@property (nonatomic) BOOL gameover;
@property (nonatomic, strong) NSMutableArray* obstacle;

-(void) setDifficulty: (NSString*) level;
-(void) newGame;
-(void) updateLocation: (int) left withRightSpeed: (int) right;
-(double) getX;
-(double) getY;
-(double) getAngle;

@end
