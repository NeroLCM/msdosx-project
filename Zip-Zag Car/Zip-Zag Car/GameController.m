//
//  GameController.m
//  Zig-Zag Car
//
//  Created by Lisa Wen on 12/10/14.
//  Copyright (c) 2014 Final Project. All rights reserved.
//

#import "GameController.h"
#import <math.h>
#import <stdlib.h>
#import "ZZObstacle.h"

@implementation GameController

@synthesize score;
@synthesize gameover;
@synthesize obstacle;
@synthesize succeed;
@synthesize levels;

#define degrad 0.017453292519943295
#define raddeg 57.29577951308232

static BOOL created;
static GameController* game;


static const double turn_factor = 0.1;
static const double mass = 4;
static const double inertia = 4 * mass;
static const int BLOCK = 500;
static const int ROAD_WIDTH = 200;
static const int OBS_WIDTH = 25;
static const int OBS_HEIGHT = 25;
static const int CAR_SIZE = 25;

double angle, vx, vy, omega;
double locX, locY;

+(GameController*) alloc {
    if (created) {
        return game;
    } else {
        game = [super alloc];
        return game;
    }
}

-(GameController*) init {
    self = [super init];
    created = YES;
    score = 0;
    angle = omega = 0;
    vx = vy = 0;
    locX = locY = 0;
    gameover = NO;
    succeed = NO;
    return self;
}

-(GameController*) initWithLocationX:(int) x andLocationY:(int) y andAngle:(double) ang{
    self = [self init];
    locX = x;
    locY = y;
    angle = ang;
    return self;
}

-(void) setDifficulty: (NSString*) difficulty {
    if ([difficulty isEqualToString:@"Hardestest"]){
        levels = 5;
    }
    else if ([difficulty isEqualToString:@"Hardester"]) {
        levels = 4;
    } else if ([difficulty isEqualToString:@"Hardest"]) {
        levels = 3;
    } else if ([difficulty isEqualToString:@"Harder"]) {
        levels = 2;
    } else if ([difficulty isEqualToString:@"Hard"]) {
        levels = 1;
    }
}

-(void) newGame {
    score = 0;
    angle = omega = 0;
    vx = vy = 0;
    gameover = NO;
    succeed = NO;
    locX = 150;
    locY = 100;
    if(levels == 1){
        angle = arc4random_uniform(40) - 20;
    }
    else if (levels == 2 || levels == 4) {
        angle = arc4random_uniform(90) - 45;
    } else if (levels == 5){
        angle = arc4random_uniform(120) - 60;
    }
    if (angle < 0) {
        angle += 360;
    }
    [self generateObstacleArray];
}

-(void) updateLocation: (int) left withRightSpeed: (int) right{
    /*
    if (gameover) {
        return;
    }
     */
    double a = (left + right) / mass;
    double alpha = (left - right) * turn_factor / inertia;
    double angle_rad = angle * degrad;
    double ax = a * cos(angle_rad);
    double ay = a * sin(angle_rad);
    locX += (vx + 0.5 * ax * TIME) * TIME;
    locY += (vy + 0.5 * ay * TIME) * TIME;
    angle += (0.5 * alpha * TIME + omega) * TIME * raddeg;
    while(angle > 360) angle -= 360;
    vx += ax * TIME;
    vy += ay * TIME;
    omega += alpha * TIME;
    score += vy >= 0? sqrt(vx * vx + vy * vy) : sqrt(vx * vx);
    if ([self detectCrash]) {
        //NSLog(@"Game Over");
        gameover = YES;
    }
    
    if (locX > 5950) {
        succeed = YES;
    }
    //NSLog([NSString stringWithFormat:@"accelaration: %f, vx: %f, vy: %f, x: %f, y: %f, angle: %f, omega: %f, alpha: %f", a, vx, vy, locX, locY, angle, omega, alpha]);
}

-(void) generateObstacleArray{
    obstacle = [[NSMutableArray alloc]init];
    int density = 0;
    if (levels > 2){
        
        if (levels == 3) {
            density = 3;
        } else if (levels == 4){
            density = 8;
        } else {
            density = 10;
        }
        
        for (int i = 500; i < 5500; i += BLOCK) {
            for (int num = 0; num < density; num++) {
                int randy = arc4random_uniform(BLOCK);
                int randx = arc4random_uniform(ROAD_WIDTH - OBS_WIDTH);
                [obstacle addObject: [[ZZObstacle alloc] initWithX:randx Y:i + randy Width:OBS_WIDTH Height:OBS_HEIGHT]];
            }
        }
    }
    // print item
    NSLog(@"Obstacles");
    for (int i=0; i < [obstacle count]; i++) {
        ZZObstacle* obs = [obstacle objectAtIndex:i];
        //NSLog([NSString stringWithFormat:@"x=%d, y=%d, h=%d, w=%d", obs.x, obs.y, obs.w, obs.h]);
    }
}

-(BOOL)detectCrash{
    double ptx, pty;
    double a_cos = cos(degrad * angle);
    double a_sin = sin(degrad * angle);
    
    ptx = - CAR_SIZE * a_cos - CAR_SIZE * a_sin + locY;
    pty = - CAR_SIZE * a_sin + CAR_SIZE * a_cos + locX;
    if (ptx <= 0 || ptx >= ROAD_WIDTH || pty <= 0) {
        return YES;
    }
    ptx = CAR_SIZE * a_cos - CAR_SIZE * a_sin + locY;
    pty = CAR_SIZE * a_sin + CAR_SIZE * a_cos + locX;
    if (ptx <= 0 || ptx >= ROAD_WIDTH || pty <= 0) {
        return YES;
    }
    ptx = - CAR_SIZE * a_cos + CAR_SIZE * a_sin + locY;
    pty = - CAR_SIZE * a_sin - CAR_SIZE * a_cos + locX;
    if (ptx <= 0 || ptx >= ROAD_WIDTH || pty <= 0) {
        return YES;
    }
    ptx = CAR_SIZE * a_cos + CAR_SIZE * a_sin + locY;
    pty = CAR_SIZE * a_sin - CAR_SIZE * a_cos + locX;
    if (ptx <= 0 || ptx >= ROAD_WIDTH || pty <= 0) {
        return YES;
    }
    
    for (int i = 0; i < [obstacle count]; i++) {
        ZZObstacle* obs = [obstacle objectAtIndex:i];
        ptx = - CAR_SIZE * a_cos - CAR_SIZE * a_sin + locY;
        pty = - CAR_SIZE * a_sin + CAR_SIZE * a_cos + locX;
        if (ptx >= obs.x && ptx <= obs.x + obs.w && pty >= obs.y && pty <= obs.y + obs.h) {
            return YES;
        }
        ptx = CAR_SIZE * a_cos - CAR_SIZE * a_sin + locY;
        pty = CAR_SIZE * a_sin + CAR_SIZE * a_cos + locX;
        if (ptx >= obs.x && ptx <= obs.x + obs.w && pty >= obs.y && pty <= obs.y + obs.h) {
            return YES;
        }
        ptx = - CAR_SIZE * a_cos + CAR_SIZE * a_sin + locY;
        pty = - CAR_SIZE * a_sin - CAR_SIZE * a_cos + locX;
        if (ptx >= obs.x && ptx <= obs.x + obs.w && pty >= obs.y && pty <= obs.y + obs.h) {
            return YES;
        }
        ptx = CAR_SIZE * a_cos + CAR_SIZE * a_sin + locY;
        pty = CAR_SIZE * a_sin - CAR_SIZE * a_cos + locX;
        if (ptx >= obs.x && ptx <= obs.x + obs.w && pty >= obs.y && pty <= obs.y + obs.h) {
            return YES;
        }
        ptx = CAR_SIZE * a_sin + locY;
        pty = CAR_SIZE * a_cos + locX;
        if (ptx >= obs.x && ptx <= obs.x + obs.w && pty >= obs.y && pty <= obs.y + obs.h) {
            return YES;
        }
        ptx = - CAR_SIZE * a_sin + locY;
        pty = - CAR_SIZE * a_cos + locX;
        if (ptx >= obs.x && ptx <= obs.x + obs.w && pty >= obs.y && pty <= obs.y + obs.h) {
            return YES;
        }
    }
    return NO;
}


-(void) updateLocation {
    return [self updateLocation: 0 withRightSpeed: 0];
}

-(double) getX{
    return locX;
}

-(double) getY{
    return locY;
}

-(double) getAngle{
    return angle;
}

@end
