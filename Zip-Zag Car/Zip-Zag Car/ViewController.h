//
//  ViewController.h
//  Zig-Zag Car
//
//  Created by Lisa Wen on 12/10/14.
//  Copyright (c) 2014 Awww. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

@interface ViewController : UIViewController <AVAudioPlayerDelegate>

@property (nonatomic, strong) IBOutlet UIView *launcher;
@property (nonatomic, strong) IBOutlet UIView *gameOver;
@property (strong, nonatomic) IBOutlet UIView *success;

@property (strong, nonatomic) IBOutlet UIButton *instruction;
@property (strong, nonatomic) IBOutlet UIButton *gotIt;
@property (strong, nonatomic) IBOutlet UIVisualEffectView *instructionPlane;
@property (strong, nonatomic) IBOutlet UITextView *instructionText;

@property (nonatomic, strong) IBOutlet UIButton *createGame;
@property (nonatomic, strong) IBOutlet UIPickerView *selectMode;
@property (nonatomic, strong) IBOutlet UILabel *selectDifficulty;
@property (strong, nonatomic) IBOutlet UIButton *startGame;

@property (strong, nonatomic) IBOutlet UIButton *playAgain;
@property (strong, nonatomic) IBOutlet UILabel *gameOverLabel;
@property (strong, nonatomic) IBOutlet UILabel *successLabel;

@property (nonatomic, strong) IBOutlet UISlider *sliderLeft;
@property (nonatomic, strong) IBOutlet UISlider *sliderRight;
@property (nonatomic, strong) IBOutlet UILabel *score;
@property (strong, nonatomic) IBOutlet UIButton *pause;
@property (strong, nonatomic) IBOutlet UIButton *resume;
@property (strong, nonatomic) AVAudioPlayer *audioPlayer;
@property (strong, nonatomic) AVAudioPlayer *soundPlayer;


@property (strong, nonatomic) UIView *roadView;


- (IBAction)newGame:(id)sender;

- (IBAction)viewInstructions:(id)sender;

- (IBAction)start:(id)sender;

- (IBAction)pauseGame:(id)sender;
- (IBAction)resumeGame:(id)sender;

- (IBAction)restartGame:(id)sender;

-(void) timerCycle:(NSTimer *) timer;
@end

