//
//  ViewController.m
//  Zig-Zag Car
//
//  Created by Lisa Wen on 12/10/14.
//  Copyright (c) 2014 Awww. All rights reserved.
//

#import "ViewController.h"
#import "GameController.h"
#import "ZZObstacle.h"
#import <AVFoundation/AVFoundation.h>

@interface ViewController ()
{
    NSArray *_pickerData;
    GameController *activeGame;
    NSString *difficulty;
    BOOL paused;
    NSTimer* timer;
    UIView *topCover, *botCover;
    UIImageView *car;
    BOOL notPlayed;
    
    double locX, locY, angle;
}

@end

@implementation ViewController
@synthesize audioPlayer;
@synthesize soundPlayer;


- (BOOL) shouldAutorotate{
    return NO;
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    notPlayed = YES;
    
    NSString *soundFilePath = [[NSBundle mainBundle] pathForResource:@"zig_zag_car" ofType:@"wav"];
    NSURL *soundFileURL = [NSURL fileURLWithPath:soundFilePath];
    audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:soundFileURL error:nil];
    audioPlayer.numberOfLoops = 0;
    [audioPlayer play];

    
    activeGame = [[GameController alloc] init];
    
    paused = NO;
    int width = self.view.frame.size.width;
    int height = self.view.frame.size.height;
    
    
    _launcher.frame = self.view.frame;
    _gameOver.frame = self.view.frame;
    _success.frame = self.view.frame;
    
    
    // Button Frames.
    CGRect startBtnFrame = [_createGame frame];
    startBtnFrame.origin.x = width / 2 - startBtnFrame.size.width / 2;
    startBtnFrame.origin.y = height / 3 - startBtnFrame.size.height / 2;
    [_createGame setFrame:startBtnFrame];
    
    CGRect resumeBtnFrame = [_createGame frame];
    resumeBtnFrame.origin.x = width / 2 - resumeBtnFrame.size.width / 2;
    resumeBtnFrame.origin.y = height / 3 + resumeBtnFrame.size.height ;
    [_resume setFrame:resumeBtnFrame];
    
    
    // Views on launcher.
    CGRect pickerFrame = [_selectMode frame];
    pickerFrame.origin.x = width / 2 - pickerFrame.size.width / 2;
    pickerFrame.origin.y = height / 4 - pickerFrame.size.height / 2 + startBtnFrame.size.height * 3;
    [_selectMode setFrame:pickerFrame];
    
    CGRect GOFrame = [_startGame frame];
    GOFrame.origin.x = width / 2 - GOFrame.size.width / 2;
    GOFrame.origin.y = height / 4 - GOFrame.size.height / 2 + pickerFrame.size.height + startBtnFrame.size.height * 2;
    [_startGame setFrame:GOFrame];
    
    CGRect labelFrame = [_selectDifficulty frame];
    labelFrame.origin.x = width / 2 - labelFrame.size.width / 2;
    labelFrame.origin.y = height / 4 - labelFrame.size.height / 2;
    [_selectDifficulty setFrame:labelFrame];
    
    CGRect instFrame = [_instruction frame];
    instFrame.origin.x = width / 2 - instFrame.size.width / 2;
    instFrame.origin.y = 3 * height / 4 - instFrame.size.height / 2;
    [_instruction setFrame:instFrame];
    
    
    // Views on instruction plane.
    CGRect gotItFrame = [_gotIt frame];
    gotItFrame.origin.x = width / 2 - gotItFrame.size.width / 2;
    gotItFrame.origin.y = 3 * height / 4 - gotItFrame.size.height / 2;
    [_gotIt setFrame:gotItFrame];
    
    CGRect textFrame = [_instructionText frame];
    textFrame.origin.x = width / 2 - textFrame.size.width / 2;
    textFrame.origin.y = 0.4 * height - textFrame.size.height / 2;
    [_instructionText setFrame:textFrame];
    
    
    // Views on success / gameOver.
    CGRect gameOverLabelFrame = [_gameOverLabel frame];
    gameOverLabelFrame.origin.x = width / 2 - gameOverLabelFrame.size.width / 2;
    gameOverLabelFrame.origin.y = height / 3 - gameOverLabelFrame.size.height / 2;
    [_gameOverLabel setFrame:gameOverLabelFrame];
    
    CGRect successLabelFrame = [_successLabel frame];
    successLabelFrame.origin.x = width / 2 - successLabelFrame.size.width / 2;
    successLabelFrame.origin.y = height / 3 - successLabelFrame.size.height / 2;
    [_successLabel setFrame:successLabelFrame];
    
    CGRect playAgainFrame = [_playAgain frame];
    playAgainFrame.origin.x = width / 2 - playAgainFrame.size.width / 2;
    playAgainFrame.origin.y = 2 * height / 3 - playAgainFrame.size.height / 2;
    [_playAgain setFrame:playAgainFrame];
    
    
    // Sliders.
    CGAffineTransform sliderRotation = CGAffineTransformIdentity;
    CGAffineTransform sliderRotateLeft = CGAffineTransformRotate(sliderRotation, 270.0 / 180 * M_PI);
    CGAffineTransform sliderRotateRight = CGAffineTransformRotate(sliderRotation, -(M_PI / 2));
    _sliderLeft.transform = sliderRotateLeft;
    _sliderRight.transform = sliderRotateRight;
    
    CGRect leftFrame = [_sliderLeft frame];
    leftFrame.size.height = leftFrame.size.height * 1.2;
    leftFrame.origin.x = width / 8 - leftFrame.size.width / 2;
    leftFrame.origin.y = 0.4 * height - leftFrame.size.height / 2 + 100;
    [_sliderLeft setFrame:leftFrame];
    
    CGRect rightFrame = [_sliderRight frame];
    rightFrame.size.height = rightFrame.size.height * 1.2;
    rightFrame.origin.x = 7 * width / 8 - rightFrame.size.width / 2;
    rightFrame.origin.y = 0.4 * height - rightFrame.size.height / 2 + 100;
    [_sliderRight setFrame:rightFrame];
    
    
    // Runtime Frames.
    CGRect scoreFrame = [_score frame];
    scoreFrame.origin.x = 0.6 * width - scoreFrame.size.width / 2;
    scoreFrame.origin.y = height / 12 - scoreFrame.size.height / 2;
    [_score setFrame:scoreFrame];
    
    CGRect pauseFrame = [_pause frame];
    pauseFrame.origin.x = width - pauseFrame.size.width * 1.2;
    pauseFrame.origin.y = 0.7 * pauseFrame.size.height;
    [_pause setFrame:pauseFrame];
    
    
    // Utilities
    topCover = [[UIView alloc] init];
    CGRect topCoverFrame = [topCover frame];
    topCoverFrame.origin = self.view.frame.origin;
    topCoverFrame.size.height = height / 7;
    topCoverFrame.size.width = width;
    topCover.backgroundColor = [UIColor whiteColor];
    [topCover setFrame:topCoverFrame];
    
    botCover = [[UIView alloc] init];
    CGRect botCoverFrame = [botCover frame];
    botCoverFrame.origin.x = self.view.frame.origin.x;
    botCoverFrame.origin.y = 8 * height / 9;
    botCoverFrame.size.height = height / 9 + 1;
    botCoverFrame.size.width = width;
    botCover.backgroundColor = [UIColor whiteColor];
    [botCover setFrame:botCoverFrame];
    
    
    // Load Views.
    [self loadRoad];
    [self loadLauncher];
    
    [_gameOver removeFromSuperview];
    [_success removeFromSuperview];
    
}

- (void) loadLauncher {
    
    [_launcher addSubview:_createGame];
    [_launcher addSubview:_instruction];
    
    if(paused){
        [_launcher addSubview:_resume];
    } else{
        [_resume removeFromSuperview];
    }
    
    [_selectMode removeFromSuperview];
    [_selectDifficulty removeFromSuperview];
    [_startGame removeFromSuperview];
    [_instructionPlane removeFromSuperview];
    
    [self.view addSubview:_launcher];
    [self.view bringSubviewToFront:_launcher];
}

- (void) loadRoad {
    
    _roadView = [[UIView alloc] init];
    
    CGRect roadFrame = [_roadView frame];
    
    _roadView.layer.borderColor = [UIColor grayColor].CGColor;
    _roadView.layer.borderWidth = 4.f;
    
    roadFrame.size.width = 200;
    roadFrame.size.height = 6000;
    roadFrame.origin.x = self.view.frame.size.width / 2 - roadFrame.size.width / 2;
    roadFrame.origin.y = self.view.frame.size.height - roadFrame.size.height;
    
    [_roadView setFrame:roadFrame];
    
    
    for (int i = 0; i <= roadFrame.size.height; i += 500) {
        UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, i, roadFrame.size.width, 1.5)];
        lineView.backgroundColor = [UIColor lightGrayColor];
        [_roadView addSubview:lineView];
    }
    
    
    [self.view addSubview:_roadView];
    
    [self.view addSubview:topCover];
    [self.view bringSubviewToFront:topCover];
    [self.view addSubview:botCover];
    [self.view bringSubviewToFront:botCover];
    
    [self.view bringSubviewToFront:_score];
    [self.view bringSubviewToFront:_pause];
}

- (void) loadCar {
    
    locX = [activeGame getX];
    locY = [activeGame getY];
    angle = [activeGame getAngle];
    
    car = [[UIImageView alloc] initWithFrame:CGRectMake(locY - 25, _roadView.frame.size.height - locX - 25, 50, 50)];

    [car setImage: [UIImage imageNamed:@"car.png" ]];
    
    car.layer.anchorPoint = CGPointMake(0.5, 0.5);
    car.transform = CGAffineTransformMakeRotation(angle / 180 * M_PI);
    
    [_roadView addSubview:car];
}

- (void) loadObstacles {
    NSMutableArray* obstacles = [activeGame obstacle];
    for (int i = 0; i < obstacles.count; i ++) {
        ZZObstacle* obstacle = (ZZObstacle*) [obstacles objectAtIndex:i];
        UIImageView* obstacleView = [[UIImageView alloc] initWithFrame:CGRectMake(obstacle.x, _roadView.frame.size.height - obstacle.y, obstacle.w, obstacle.h)];
        [obstacleView setImage: [UIImage imageNamed:[NSString stringWithFormat:@"obs%d.png", (i % 3 + 1)]]];
        [_roadView addSubview:obstacleView];
        
    }
}

- (void) updateCar {
    locX = [activeGame getX];
    locY = [activeGame getY];
    angle = [activeGame getAngle];
    car.transform = CGAffineTransformMakeRotation(0);
    CGRect carFrame = [car frame];
    carFrame.origin.y = _roadView.frame.size.height - locX - 25;
    carFrame.origin.x = locY - 25;
    car.frame = carFrame;
    car.transform = CGAffineTransformMakeRotation(angle / 180 * M_PI);
    //NSLog([NSString stringWithFormat:@"angle: %f", angle]);
    
    [_roadView bringSubviewToFront:car];
}

- (void) loadGameOver {
    [timer invalidate];
    timer = nil;
    NSString *soundFilePath = [[NSBundle mainBundle] pathForResource:@"game_over" ofType:@"wav"];
    NSURL *soundFileURL = [NSURL fileURLWithPath:soundFilePath];
    audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:soundFileURL error:nil];
    audioPlayer.numberOfLoops = 0;
    [audioPlayer play];
    
    [soundPlayer stop];
    [_gameOver addSubview:_gameOverLabel];
    [_gameOver addSubview:_playAgain];
    
    [self.view addSubview:_gameOver];
    [self.view bringSubviewToFront:_gameOver];
}

- (void) loadSuccess {
    NSString *soundFilePath = [[NSBundle mainBundle] pathForResource:@"pass" ofType:@"wav"];
    NSURL *soundFileURL = [NSURL fileURLWithPath:soundFilePath];
    audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:soundFileURL error:nil];
    audioPlayer.numberOfLoops = 0;
    [audioPlayer play];
    [timer invalidate];
    timer = nil;
    
    [_success addSubview:_successLabel];
    [_success addSubview:_playAgain];
    
    [self.view addSubview:_success];
    [self.view bringSubviewToFront:_success];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)newGame:(id)sender {
    [soundPlayer stop];
    NSString *soundFilePath = [[NSBundle mainBundle] pathForResource:@"new_game" ofType:@"wav"];
    NSURL *soundFileURL = [NSURL fileURLWithPath:soundFilePath];
    audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:soundFileURL error:nil];
    audioPlayer.numberOfLoops = 0;
    [audioPlayer play];
    _pickerData = @[@"Hard", @"Harder", @"Hardest", @"Hardester", @"Hardestest"];
    _selectMode.dataSource = self;
    _selectMode.delegate = self;
    
    [_createGame removeFromSuperview];
    [_resume removeFromSuperview];
    [_instruction removeFromSuperview];
    paused = NO;
    
    [_launcher addSubview:_selectMode];
    [_launcher addSubview:_selectDifficulty];
    [_launcher addSubview:_startGame];
}

- (IBAction)viewInstructions:(id)sender {
    _instructionPlane = [[UIVisualEffectView alloc] initWithEffect: [UIBlurEffect effectWithStyle: UIBlurEffectStyleDark]];
    _instructionPlane.frame = self.view.frame;
    
    [_instructionPlane addSubview:_gotIt];
    [_instructionPlane addSubview:_instructionText];
    
    [_launcher addSubview:_instructionPlane];
}

- (IBAction)start:(id)sender {
    NSString *soundFilePath = [[NSBundle mainBundle] pathForResource:@"begin" ofType:@"wav"];
    NSURL *soundFileURL = [NSURL fileURLWithPath:soundFilePath];
    audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:soundFileURL error:nil];
    audioPlayer.numberOfLoops = 0;
    [audioPlayer play];
    notPlayed = NO;
    [_sliderLeft setValue:0];
    [_sliderRight setValue:0];
    [_launcher removeFromSuperview];
    [_roadView removeFromSuperview];
    
    [activeGame setDifficulty:difficulty];
    [activeGame newGame];
    
    [self loadRoad];
    [self loadCar];
    [self loadObstacles];
    int level = [activeGame levels];
    //NSLog([NSString stringWithFormat:@"%d",level]);
    NSString *bgPath;
    if (level <=2) {
        bgPath = [[NSBundle mainBundle] pathForResource:@"bg1" ofType:@"m4a"];
    } else if (level <= 4){
        bgPath = [[NSBundle mainBundle] pathForResource:@"bg2" ofType:@"m4a"];
    } else {
        bgPath = [[NSBundle mainBundle] pathForResource:@"bg3" ofType:@"m4a"];
    }
    NSURL *bgURL = [NSURL fileURLWithPath:bgPath];
    soundPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:bgURL error:nil];
    soundPlayer.numberOfLoops = -1;
    soundPlayer.volume = 0.7f;
    [soundPlayer play];
    timer = [NSTimer scheduledTimerWithTimeInterval:TIME target:self selector:@selector(timerCycle:) userInfo:nil repeats:YES];
}

- (IBAction)pauseGame:(id)sender {
    if ([activeGame gameover] || notPlayed) {
        return;
    }
    paused = YES;
    [timer invalidate];
    timer = nil;
    [self loadLauncher];
}

- (IBAction)resumeGame:(id)sender {
    paused = NO;
    timer = [NSTimer scheduledTimerWithTimeInterval:TIME target:self selector:@selector(timerCycle:) userInfo:nil repeats:YES];
    
    [_launcher removeFromSuperview];
}

- (IBAction)restartGame:(id)sender {
    NSString *soundFilePath = [[NSBundle mainBundle] pathForResource:@"again" ofType:@"wav"];
    NSURL *soundFileURL = [NSURL fileURLWithPath:soundFilePath];
    audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:soundFileURL error:nil];
    audioPlayer.numberOfLoops = 0;
    [audioPlayer play];
    [_success removeFromSuperview];
    [_gameOver removeFromSuperview];
    
    [_selectMode removeFromSuperview];
    [_selectDifficulty removeFromSuperview];
    [_startGame removeFromSuperview];
    [_instructionPlane removeFromSuperview];
    
    _pickerData = @[@"Hard", @"Harder", @"Hardest", @"Hardester", @"Hardestest"];
    _selectMode.dataSource = self;
    _selectMode.delegate = self;
    
    [_createGame removeFromSuperview];
    [_resume removeFromSuperview];
    [_instruction removeFromSuperview];
    paused = NO;
    
    [_launcher addSubview:_selectMode];
    [_launcher addSubview:_selectDifficulty];
    [_launcher addSubview:_startGame];
    [self.view addSubview:_launcher];
    [self.view bringSubviewToFront:_launcher];
}


// The following 5 methods are for delegation of picker.
// The number of columns of data
- (int)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

// The number of rows of data
- (int)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return _pickerData.count;
}

// The data to return for the row and component (column) that's being passed in
- (NSString*)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return _pickerData[row];
}

// Catpure the picker view selection
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    // This method is triggered whenever the user makes a change to the picker selection.
    // The parameter named row and component represents what was selected.
    difficulty = _pickerData[row];
}

-(UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view{
    
    UILabel *label = [[UILabel alloc] init];
    
    label.textColor = [UIColor whiteColor];
    label.font = [UIFont fontWithName:@"Noteworthy-Light" size:20];\
    
    label.text = _pickerData[row];
    label.textAlignment = NSTextAlignmentCenter;
    
    return label;
}


- (void) timerCycle:(NSTimer *) timer{
    [activeGame updateLocation: (int)_sliderLeft.value withRightSpeed:(int) _sliderRight.value];
    [[self score] setText:[NSString stringWithFormat:@"Score: %d", [activeGame score]]];
    
    [self updateCar];
    
    CGRect roadFrame = [_roadView frame];
    roadFrame.origin.y = self.view.frame.size.height - roadFrame.size.height + locX - 150;
    
    [_roadView setFrame:roadFrame];
    
    if ([activeGame succeed]) {
        [self loadSuccess];
    }
    
    if (activeGame.gameover) {
        [self loadGameOver];
    }
    
    return;
}
@end
