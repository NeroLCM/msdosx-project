//
//  ZZObstacle.h
//  Zig-Zag Car
//
//  Created by Nero on 12/11/14.
//  Copyright (c) 2014 Awww. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ZZObstacle : NSObject
    @property (nonatomic) int x;
    @property (nonatomic) int y;
    @property (nonatomic) int w;
    @property (nonatomic) int h;

-(ZZObstacle*)initWithX:(int) _x Y:(int) _y Width:(int) _w Height:(int) _h;
@end
