//
//  ZZObstacle.m
//  Zig-Zag Car
//
//  Created by Nero on 12/11/14.
//  Copyright (c) 2014 Awww. All rights reserved.
//

#import "ZZObstacle.h"

@implementation ZZObstacle

-(ZZObstacle*)initWithX:(int) x Y:(int) y Width:(int) w Height:(int) h{
    self = [super init];
    _x = x;
    _y = y;
    _w = w;
    _h = h;
    return self;
}

@end
