//
//  main.m
//  Zip-Zag Car
//
//  Created by Lisa Wen on 12/10/14.
//  Copyright (c) 2014 Awww. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
